module.exports = {
   presets: ['module:metro-react-native-babel-preset'],
   plugins: [
      [
         'module-resolver',
         {
            root: ['./src'],
            alias: {
               '@src': './src',
               '@assets': './src/assets',
               '@config': './src/config',
               '@components': './src/components',
               '@constants': './src/constants',
               '@store': './src/store',
               '@pages': './src/pages',
               '@routers': './src/routers',
               '@utils': './src/utils',
            },
         },
      ],
      [
         'module:react-native-dotenv',
         {
            moduleName: '@env',
            path: '.env.development',
            safe: false,
            allowUndefined: true,
         },
      ],
   ],
};
