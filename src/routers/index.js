import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Guest, Home, Splash } from '@pages';

const Routers = () => {
   const Stack = createNativeStackNavigator();

   return (
      <Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown: false }}>
         <Stack.Screen name="Splash" component={Splash} />
         <Stack.Screen name="Home" component={Home} />
         <Stack.Screen name="Guest" component={Guest} />
      </Stack.Navigator>
   );
};

export default Routers;
