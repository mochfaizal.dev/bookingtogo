import IconLogo from './ic_logo.svg';
import IconRefund from './ic_refund.svg';
import IconUserMale from './ic_user_male.svg';
import IconUserFemale from './ic_user_female.svg';
import IconLeftArrow from './ic_left_arrow.svg';
import IconChevronDown from './ic_chevron_down.svg';
import IconTrash from './ic_trash.svg';

export {
   IconLogo,
   IconRefund,
   IconUserMale,
   IconUserFemale,
   IconLeftArrow,
   IconChevronDown,
   IconTrash,
};
