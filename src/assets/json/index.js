import GuestStatic from './Guest.json';
import TabStatic from './Tab.json';
import GenderStatic from './Gender.json';

export { GuestStatic, TabStatic, GenderStatic };
