import { IS_LOADING } from '@constants/initType';

export const setLoading = (payload) => ({
   type: IS_LOADING,
   payload,
});
