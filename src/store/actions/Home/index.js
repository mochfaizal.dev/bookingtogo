import { requestGet } from '@config';
import { GET_PAYMENT_DETAIL } from '@constants/initHttp';
import { STORE_PAYMENT_DETAIL } from '@constants/initType';
import { setLoading } from '../../actions';

export const getPaymentDetail = (paymentId) => {
   return async (dispatch) => {
      dispatch(setLoading(true));
      try {
         let res = await requestGet(`${GET_PAYMENT_DETAIL}${paymentId}`);
         dispatch(setPaymentDetail(res));
      } catch (error) {
         throw error;
      }
      dispatch(setLoading(false));
   };
};

export const setPaymentDetail = (payload) => ({
   type: STORE_PAYMENT_DETAIL,
   payload,
});
