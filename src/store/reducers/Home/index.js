import { STORE_PAYMENT_DETAIL } from '@constants/initType';

const initState = {
   paymentDetail: false,
};

const Home = (state = initState, action) => {
   switch (action.type) {
      case STORE_PAYMENT_DETAIL:
         return {
            ...state,
            paymentDetail: action.payload,
         };

      default:
         return state;
   }
};

export default Home;
