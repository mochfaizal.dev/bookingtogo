import { IS_LOADING } from '@constants/initType';

const initState = {
   loading: false,
};

const General = (state = initState, action) => {
   switch (action.type) {
      case IS_LOADING:
         return {
            ...state,
            loading: action.payload,
         };

      default:
         return state;
   }
};

export default General;
