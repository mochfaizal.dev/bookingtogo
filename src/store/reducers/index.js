import { combineReducers } from 'redux';
import General from './General';
import Home from './Home';

const reducers = combineReducers({ Home, General });

export default reducers;
