import { StyleSheet } from 'react-native';
import { Colors } from '@utils';

export default StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.main,
   },
});
