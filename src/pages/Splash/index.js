import React, { useEffect } from 'react';
import { Animated, View } from 'react-native';
import { storeData } from '@utils';
import { APPLICATION_ID } from '@env';
import styles from './Splash.pages.style';

const Splash = (props) => {
   const { navigation } = props;
   const width = new Animated.Value(80);

   useEffect(() => {
      Animated.timing(width, {
         toValue: 280,
         duration: 2000,
         useNativeDriver: false,
      }).start();

      const staticApplicationId = setTimeout(() => {
         storeData('applicationId', APPLICATION_ID);
         navigation.replace('Home');
      }, 2000);

      return () => clearTimeout(staticApplicationId);
   }, []);

   return (
      <View style={styles.container}>
         <Animated.Image
            source={require('@assets/img/img_logo.png')}
            style={{ width: width, height: 120, position: 'absolute', resizeMode: 'contain' }}
         />
      </View>
   );
};

export default Splash;
