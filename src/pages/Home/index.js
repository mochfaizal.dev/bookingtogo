import React, { createRef, useEffect, useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import { TabStatic, GuestStatic } from '@assets/json';
import { Toolbar, Typography, Tab, Gap, Line, CardHotel, Card, RadioButton } from '@components';
import { Colors, translateGender } from '@utils';
import styles from './Home.pages.style';
import { useDispatch, useSelector } from 'react-redux';
import { getPaymentDetail } from '@store/actions';
import dayjs from 'dayjs';
import { IconRefund, IconUserFemale, IconUserMale } from '@src/assets/illustrations';

const Home = (props) => {
   const tabRef = createRef();
   const dispatch = useDispatch();
   const { Home } = useSelector((state) => state);
   const { navigation } = props;
   const [tab, setTab] = useState(TabStatic.data);
   const [guest, setGuest] = useState(GuestStatic);
   const [hotelDetail, setHotelDetail] = useState(false);
   const [optionOrder, setOptionOrder] = useState('self');

   useEffect(() => {
      dispatch(getPaymentDetail('bVonXoSUHK'));
   }, []);

   useEffect(() => {
      if (Home.paymentDetail) {
         setHotelDetail(Home.paymentDetail.chosen_hotel.data.get_chosen_hotel);
      }
   }, [Home.paymentDetail]);

   const setActive = (data, filterBy, updatedValue) => {
      let currentIndex = 0;
      const newState = data.map((obj, i) => {
         if (obj.name === filterBy) {
            currentIndex = i;
            return {
               ...obj,
               is_active: updatedValue,
            };
         }

         return { ...obj, is_active: false };
      });
      tabRef.current.scrollToIndex({ index: currentIndex, animated: true, viewPosition: 0.5 });
      setTab(newState);
   };

   const onSaveData = (params) => {
      setGuest(params);
   };

   return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.background.white }}>
         <Toolbar>
            <Typography
               size={14}
               color={Colors.background.white}
               style={{ flex: 1, textAlign: 'center' }}
            >
               Payment Details
            </Typography>
         </Toolbar>
         <View style={styles.tab}>
            <Tab ref={tabRef} data={tab} onClick={(name) => setActive(tab, name, true)} />
         </View>
         <View style={{ flex: 1, maxHeight: 1, backgroundColor: Colors.line }} />
         <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
            <View>
               {hotelDetail && (
                  <View style={styles.body}>
                     <Typography size={13} color={Colors.text.primary} weight={500}>
                        Detail Pesanan
                     </Typography>
                     <Gap height={10} />
                     <CardHotel data={hotelDetail} />
                     <Gap height={20} />
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Typography size={13} weight={600}>
                           Check-In
                        </Typography>
                        <Typography size={12} color={Colors.text.secondary}>
                           {dayjs(hotelDetail.chosen_hotel_params.check_in).format('DD MMMM YYYY')}
                        </Typography>
                     </View>
                     <Gap height={10} />
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Typography size={13} weight={600}>
                           Check-Out
                        </Typography>
                        <Typography size={12} color={Colors.text.secondary}>
                           {dayjs(hotelDetail.chosen_hotel_params.check_out).format('DD MMMM YYYY')}
                        </Typography>
                     </View>
                     <Gap height={10} />
                     <View style={styles.refund}>
                        <IconRefund width={16} height={16} />
                        <Gap width={12} />
                        <Typography size={12} color={Colors.secondary} weight={500}>
                           Dapat direfund jika dibatalkan.
                        </Typography>
                     </View>
                  </View>
               )}
               <Line />
               <View style={styles.body}>
                  <Typography size={13} color={Colors.text.primary} weight={500}>
                     Detail Pemesan
                  </Typography>
                  <Gap height={10} />
                  <Card style={{ paddingVertical: 12 }}>
                     <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 0.8 }}>
                           <Typography size={12} weight={500}>
                              Tn. Moch Faizal
                           </Typography>
                           <Typography size={11} color={Colors.text.secondary}>
                              mochfaizal.dev@gmail.com
                           </Typography>
                           <Typography size={11} color={Colors.text.secondary}>
                              +628 22 2222 2222
                           </Typography>
                        </View>
                        <TouchableOpacity style={{ flex: 0.2 }}>
                           <Typography
                              size={12}
                              weight={500}
                              color={Colors.main}
                              style={{ textDecorationLine: 'underline' }}
                           >
                              Ubah
                           </Typography>
                        </TouchableOpacity>
                     </View>
                  </Card>
                  <Gap height={12} />
                  <RadioButton
                     text="Saya memesan untuk sendiri"
                     active={optionOrder == 'self'}
                     onPress={() => setOptionOrder('self')}
                  />
                  <Gap height={8} />
                  <RadioButton
                     text="Saya memesan untuk orang lain"
                     active={optionOrder == 'another_person'}
                     onPress={() => setOptionOrder('another_person')}
                  />
                  <Gap height={16} />
                  {optionOrder == 'another_person' && (
                     <>
                        <Typography size={13} color={Colors.text.primary} weight={500}>
                           Data Tamu
                        </Typography>
                        <Gap height={10} />
                        {guest &&
                           guest.map((item, i) => {
                              return (
                                 <View key={i}>
                                    <Card style={styles.cardGuest}>
                                       {item.gender == 'Male' ? (
                                          <IconUserMale width={16} height={16} />
                                       ) : (
                                          <IconUserFemale width={16} height={16} />
                                       )}
                                       <Gap width={12} />
                                       <Typography size={12}>{`${translateGender(item.gender)}. ${
                                          item.name
                                       }`}</Typography>
                                    </Card>
                                    {guest.length > 1 && <Gap height={10} />}
                                 </View>
                              );
                           })}
                        <Gap height={10} />
                        <TouchableOpacity
                           onPress={() => navigation.navigate('Guest', { data: guest, onSaveData })}
                           style={{ alignItems: 'flex-end' }}
                        >
                           <Typography
                              size={12}
                              style={{ textDecorationLine: 'underline' }}
                              color={Colors.main}
                           >
                              Ubah Data Tamu
                           </Typography>
                        </TouchableOpacity>
                     </>
                  )}
               </View>
            </View>
         </ScrollView>
      </View>
   );
};

export default Home;
