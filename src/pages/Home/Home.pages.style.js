import { StyleSheet, Dimensions } from 'react-native';

const { height } = Dimensions.get('window');

export default StyleSheet.create({
   tab: {
      flex: 1,
      maxHeight: height * 0.074,
   },
   container: {
      flex: 1,
      flexDirection: 'column',
   },
   body: {
      paddingHorizontal: 20,
      paddingVertical: 16,
   },
   refund: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
   },
   cardGuest: {
      paddingVertical: 12,
      flexDirection: 'row',
      alignItems: 'center',
   },
});
