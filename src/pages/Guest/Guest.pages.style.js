import { Colors } from '@src/utils';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   container: {
      flex: 1,
      flexDirection: 'column',
      paddingHorizontal: 20,
      paddingVertical: 16,
   },
   button: (disable) => ({
      backgroundColor: !disable ? Colors.background.disable : Colors.secondary,
      paddingVertical: 12,
      alignItems: 'center',
      borderRadius: 8,
      position: 'absolute',
      bottom: 20,
      width: '100%',
      marginHorizontal: 20,
   }),
});
