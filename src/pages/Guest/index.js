import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Gap, ItemGuest, Select, Toolbar, Typography } from '@components';
import { Colors } from '@utils';
import { IconLeftArrow } from '@assets/illustrations';
import styles from './Guest.pages.style';

const Guest = (props) => {
   const { navigation, route } = props;
   const { params } = route;
   const [data, setData] = useState(params.data);
   const [disable, setDisable] = useState(false);

   useEffect(() => {
      let btnDisable = false;
      if (data.length > 0) {
         btnDisable = !data.filter((val) => val.name == '').length > 0;
      }

      setDisable(btnDisable);
   }, [data]);

   return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.background.white }}>
         <Toolbar style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
               <IconLeftArrow width={20} height={20} />
            </TouchableOpacity>
            <Typography
               size={14}
               color={Colors.background.white}
               style={{ flex: 1, textAlign: 'center' }}
            >
               Tambah Data Tamu
            </Typography>
         </Toolbar>
         <View style={styles.container}>
            <Typography size={13} color={Colors.main} weight={500}>
               Data Tamu
            </Typography>
            <Gap height={12} />
            {data &&
               data.map((item, i) => {
                  return (
                     <View key={i}>
                        <ItemGuest
                           data={item}
                           index={i}
                           onUpdate={(name, gender) => {
                              let newData = [...data];
                              newData[i] = { name: name, gender: gender };
                              setData(newData);
                           }}
                           onDelete={() => {
                              let newData = [...data];
                              if (i !== -1) {
                                 newData.splice(i, 1);
                                 // console.log('data: ', newData);
                                 setData(newData);
                              }
                           }}
                        />
                        {data.length > 1 && <Gap height={10} />}
                     </View>
                  );
               })}
            <Gap height={20} />
            <TouchableOpacity onPress={() => setData([...data, { gender: 'Male', name: '' }])}>
               <Typography
                  size={12}
                  weight={500}
                  color={Colors.secondary}
                  style={{ textDecorationLine: 'underline', textAlign: 'center' }}
               >
                  + Tambah Data Tamu
               </Typography>
            </TouchableOpacity>
            <TouchableOpacity
               style={styles.button(disable)}
               disabled={!disable}
               onPress={() => {
                  params.onSaveData(data);
                  navigation.goBack();
               }}
            >
               <Typography size={13} weight={600} color={Colors.background.white}>
                  Simpan
               </Typography>
            </TouchableOpacity>
         </View>
      </View>
   );
};

export default Guest;
