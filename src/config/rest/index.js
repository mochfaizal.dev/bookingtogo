import axios from 'axios';
import { API_URL, API_KEY } from '@env';
import { objectParametize, getData } from '@utils';

const http = axios.create({
   baseURL: API_URL,
   timeout: 30000,
});

http.interceptors.request.use(
   async (config) => {
      await getData('applicationId').then((value) => {
         if (value) {
            return (config.headers['X-Parse-Application-Id'] = value.replace(/\"|\\/g, ''));
         }
      });

      config.headers['X-Parse-REST-API-Key'] = API_KEY;

      return config;
   },
   (error) => {
      return Promise.reject(error);
   }
);

export const requestPost = (url, params) => {
   return new Promise((resolve, reject) => {
      http
         .post(url, params)
         .then((response) => {
            resolve(response.data);
         })
         .catch((error) => {
            reject(error);
         });
   });
};

export const requestGet = (url, params) => {
   return new Promise((resolve, reject) => {
      http
         .get(`${url}${params ? objectParametize(params, '&', true) : ''}`)
         .then((response) => {
            resolve(response.data);
         })
         .catch((error) => {
            reject(error);
         });
   });
};

export const requestPut = (url, params, body) => {
   return new Promise((resolve, reject) => {
      http
         .put(`${API_URL}${url}${params ? objectParametize(params, '&', true) : ''}`, body)
         .then((response) => {
            resolve(response.data);
         })
         .catch((error) => {
            reject(error);
         });
   });
};

export const requestDelete = (url, body) => {
   return new Promise((resolve, reject) => {
      http
         .delete(`${url}`, {
            data: body,
         })
         .then((response) => {
            resolve(response.data);
         })
         .catch((error) => {
            reject(error);
         });
   });
};
