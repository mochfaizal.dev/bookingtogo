export const objectParametize = (obj, delimeter, q) => {
   var str = new Array();
   if (!delimeter) delimeter = '&';
   for (var key in obj) {
      switch (typeof obj[key]) {
         case 'string':
         case 'number':
            str[str.length] = key + '=' + obj[key];
            break;
         case 'object':
            str[str.length] = objectParametize(obj[key], delimeter);
      }
   }
   return (q === true ? '?' : '') + str.join(delimeter);
};

export const calculateBetweenDate = (startDate, endDate) => {
   var dateDiff = new Date(endDate).getTime() - new Date(startDate).getTime();
   return Math.floor(dateDiff / (1000 * 60 * 60 * 24));
};

export const translateGender = (gender) => {
   return gender == 'Male' ? 'Tn' : 'Ny';
};
