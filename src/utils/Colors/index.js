export const Colors = {
   main: '#335997',
   secondary: '#ff8136',
   text: {
      primary: '#3e3e3e',
      secondary: '#808080',
      placeholder: '#D1D1D1',
   },
   background: {
      white: '#FFFFFF',
      black: '#323639',
      blackTransparent: '#000000ad',
      inactive: '#d8e2eb',
      disable: '#FFE8D2',
   },
   line: '#D1D1D1',
   error: '#CC4435',
};
