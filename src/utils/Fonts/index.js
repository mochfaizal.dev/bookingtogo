export const Fonts = {
   100: 'WorkSans-Thin',
   200: 'WorkSans-ExtraLight',
   300: 'WorkSans-Light',
   400: 'WorkSans-Regular',
   500: 'WorkSans-Medium',
   600: 'WorkSans-SemiBold',
   700: 'WorkSans-Bold',
   800: 'WorkSans-Black',
};
