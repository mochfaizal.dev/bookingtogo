import React, { createRef, useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { BottomSheet, Gap, Input, Line, Select, Typography } from '@components';
import { IconTrash } from '@assets/illustrations';
import { GenderStatic } from '@assets/json';
import styles from './ItemGuest.component.style';
import { translateGender } from '@src/utils';

const ItemGuest = (props) => {
   const { data, style, index, onDelete, onUpdate } = props;
   const genderRef = createRef();
   const [field, setField] = useState(data.name);
   const [gender, setGender] = useState(data.gender);

   useEffect(() => {
      onUpdate(field, gender);
   }, [field, gender]);

   //this lifecycler for re-rendering update value on delete item.
   useEffect(() => {
      setField(data.name);
      setGender(data.gender);
   }, [data]);

   return (
      <View style={style ? [styles.root, style] : styles.root}>
         <Select
            text={data.gender}
            style={{ flex: 0.15 }}
            onPress={() => genderRef.current.open()}
         />
         <Gap width={8} />
         <Input
            placeholder="Masukkan nama anda"
            style={{ flex: index > 0 ? 0.7 : 0.85 }}
            value={field}
            onChangeText={(val) => setField(val)}
         />
         {index > 0 && (
            <TouchableOpacity
               style={{ flex: 0.15, alignItems: 'center' }}
               onPress={() => onDelete(field)}
            >
               <IconTrash width={20} height={20} />
            </TouchableOpacity>
         )}

         <BottomSheet
            ref={genderRef}
            height={180}
            container={{ paddingHorizontal: 0, paddingVertical: 8 }}
            closeOnDragDown={false}
         >
            <View style={{ flexDirection: 'column', paddingVertical: 10 }}>
               <View style={{ flexDirection: 'row', paddingHorizontal: 12 }}>
                  <Typography size={14} weight={600}>
                     Pilih Gender
                  </Typography>
               </View>
               <Gap height={12} />
               <Line />
               <Gap height={12} />
               {GenderStatic.map((val, i) => {
                  return (
                     <>
                        <TouchableOpacity
                           style={styles.selectValue}
                           key={i}
                           onPress={() => {
                              setGender(val.value);
                              genderRef.current.close();
                           }}
                        >
                           <Typography size={14}>{translateGender(val.value)}</Typography>
                        </TouchableOpacity>
                        <Gap height={12} />
                        <Line />
                        <Gap height={12} />
                     </>
                  );
               })}
            </View>
         </BottomSheet>
      </View>
   );
};

export default ItemGuest;
