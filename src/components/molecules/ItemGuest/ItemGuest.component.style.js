import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   root: {
      flexDirection: 'row',
      alignItems: 'center',
   },
   selectValue: { paddingHorizontal: 12 },
});
