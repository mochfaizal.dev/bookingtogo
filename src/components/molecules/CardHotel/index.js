import React, { useEffect } from 'react';
import { Image, View } from 'react-native';
import { BulletDot, Card, Gap, Typography } from '@components';
import { Colors, calculateBetweenDate } from '@utils';
import styles from './CardHotel.component.style';

const CardHotel = (props) => {
   const { data } = props;

   return (
      <Card style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
         <Image
            source={{ uri: data.chosen_hotel_detail.images[0].thumbnail }}
            style={styles.images}
         />
         <View style={{ flex: 0.73, flexDirection: 'column' }}>
            <Typography size={12} color={Colors.main} weight={600}>
               {data.chosen_hotel_detail.hotel_name}
            </Typography>
            <Gap height={2} />
            <Typography size={11} color={Colors.text.secondary}>
               {data.chosen_hotel_room.meal != ''
                  ? `${data.chosen_hotel_room.room_name} with ${data.chosen_hotel_room.meal}`
                  : data.chosen_hotel_room.room_name}
            </Typography>
            <Gap height={2} />
            <View
               style={{
                  flexDirection: 'row',
                  alignItems: 'center',
               }}
            >
               <Typography size={11} color={Colors.text.secondary}>
                  {`${data.chosen_hotel_params.total_room} Kamar`}
               </Typography>
               <BulletDot style={{ marginHorizontal: 4 }} />
               <Typography size={11} color={Colors.text.secondary}>
                  {`${data.chosen_hotel_params.guest_adult} Tamu`}
               </Typography>
               <BulletDot style={{ marginHorizontal: 4 }} />
               <Typography size={11} color={Colors.text.secondary}>
                  {`${calculateBetweenDate(
                     data.chosen_hotel_params.check_in,
                     data.chosen_hotel_params.check_out
                  )} Malam`}
               </Typography>
            </View>
         </View>
      </Card>
   );
};

export default CardHotel;
