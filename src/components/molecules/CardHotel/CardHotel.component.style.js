import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   images: {
      height: 70,
      resizeMode: 'cover',
      borderRadius: 8,
      flex: 0.23,
   },
});
