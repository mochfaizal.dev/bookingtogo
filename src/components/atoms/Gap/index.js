import React from 'react';
import { View } from 'react-native';

const Gap = (props) => {
   const { width, height } = props;
   return <View style={{ width: width, height: height }} />;
};

export default Gap;
