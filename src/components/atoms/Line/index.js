import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { Colors } from '@utils';

const { width } = Dimensions.get('window');
const Line = (props) => {
   const { style } = props;
   return <View style={style ? [styles.root, style] : styles.root} />;
};

export default Line;

const styles = StyleSheet.create({
   root: { width: width, height: 0.5, backgroundColor: Colors.line },
});
