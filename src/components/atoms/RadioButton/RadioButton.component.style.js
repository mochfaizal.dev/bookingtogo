import { StyleSheet } from 'react-native';
import { Colors } from '../../../utils/Colors';

const styles = (props) => {
   return StyleSheet.create({
      root: {
         flexDirection: 'row',
         alignItems: 'center',
      },
      circle: {
         width: 16,
         height: 16,
         borderRadius: 16 / 2,
         borderWidth: 1,
         borderColor: props.active ? Colors.main : Colors.line,
      },
      insideCircle: {
         flex: 1,
         borderRadius: 16 / 2,
         margin: 2,
         backgroundColor: props.active ? Colors.main : Colors.background.white,
      },
   });
};

export default styles;
