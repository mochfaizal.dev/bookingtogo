import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './RadioButton.component.style';
import Typography from '../Typography';
import { Colors } from '@utils';
import Gap from '../Gap';

const RadioButton = (props) => {
   const { text, active, style, onPress } = props;
   return (
      <TouchableOpacity
         style={style ? [styles(props).root, style] : styles(props).root}
         onPress={onPress}
      >
         <View style={styles(props).circle}>
            <View style={styles(props).insideCircle} />
         </View>
         <Gap width={10} />
         <Typography
            size={12}
            weight={active ? 500 : 400}
            color={active ? Colors.main : Colors.text.secondary}
         >
            {text}
         </Typography>
      </TouchableOpacity>
   );
};

export default RadioButton;
