import { Colors, Fonts } from '@utils';
import { StyleSheet } from 'react-native';

const styles = (props) => {
   const { size = 12, color = Colors.text.primary, weight = 400, variant = 'normal' } = props;

   return StyleSheet.create({
      root: {
         fontFamily: Fonts[weight],
         fontSize: size,
         color: color,
         fontStyle: variant,
      },
   });
};

export default styles;
