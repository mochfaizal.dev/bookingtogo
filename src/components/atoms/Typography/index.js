import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import styles from './Typography.component.style';

const Typography = (props) => {
   const { children, style } = props;

   return <Text style={style ? [styles(props).root, style] : styles(props).root}>{children}</Text>;
};

export default Typography;

Typography.propTypes = {
   size: PropTypes.number.isRequired,
   color: PropTypes.string,
   weight: PropTypes.number,
   variant: PropTypes.oneOf(['normal', 'italic']),
   style: PropTypes.object,
};
