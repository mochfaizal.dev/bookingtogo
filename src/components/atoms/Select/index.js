import React from 'react';
import styles from './Select.component.style';
import { Card, Gap, Typography } from '@components';
import { IconChevronDown } from '@src/assets/illustrations';
import { translateGender } from '@src/utils';

const Select = (props) => {
   const { text, style, onPress } = props;
   return (
      <Card style={style ? [styles.root, style] : styles.root} onClick={onPress}>
         <Typography size={12}>{translateGender(text)}</Typography>
         <Gap width={8} />
         <IconChevronDown width={12} height={12} />
      </Card>
   );
};

export default Select;
