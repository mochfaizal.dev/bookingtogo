import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   root: {
      flexDirection: 'row',
      paddingVertical: 12,
      alignItems: 'center',
   },
});
