import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors } from '@utils';

const BulletDot = (props) => {
   const { style } = props;
   return <View style={style ? [styles.root, style] : styles.root} />;
};

export default BulletDot;

const styles = StyleSheet.create({
   root: {
      width: 4,
      height: 4,
      marginTop: 3,
      borderRadius: 4 / 2,
      backgroundColor: Colors.text.secondary,
   },
});
