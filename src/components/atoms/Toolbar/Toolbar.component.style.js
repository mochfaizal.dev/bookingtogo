import { Colors } from '@src/utils';
import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
   root: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 25,
      paddingVertical: 10,
      maxHeight: 69,
      minHeight: height * 0.072,
      backgroundColor: Colors.main,
   },
});
