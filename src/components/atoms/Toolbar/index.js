import React from 'react';
import PropType from 'prop-types';
import { View } from 'react-native';
import styles from './Toolbar.component.style';

const Toolbar = (props) => {
   const { children, style } = props;
   return <View style={style ? [styles.root, style] : styles.root}>{children}</View>;
};

export default Toolbar;

Toolbar.propTypes = {
   style: PropType.oneOfType([PropType.object, PropType.array]),
};
