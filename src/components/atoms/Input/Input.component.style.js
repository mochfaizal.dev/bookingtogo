import { Colors, Fonts } from '@src/utils';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   root: {
      borderWidth: 1,
      borderRadius: 8,
      borderColor: Colors.line,
      paddingVertical: 8,
      paddingHorizontal: 8,
   },
   text: {
      fontFamily: Fonts[400],
      fontSize: 12,
      fontStyle: 'normal',
      color: Colors.text.primary,
   },
});
