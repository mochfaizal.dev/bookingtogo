import { Colors } from '@src/utils';
import React from 'react';
import { TextInput, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './Input.component.style';

const Input = (props) => {
   const { placeholder, style, ...inputProps } = props;
   return (
      <View style={style ? [styles.root, style] : styles.root}>
         <TextInput
            style={[styles.text, { flex: 1, padding: 0 }]}
            placeholder={placeholder}
            placeholderTextColor={Colors.text.placeholder}
            autoCapitalize="none"
            autoCorrect={false}
            selectionColor={Colors.main}
            {...inputProps}
         />
      </View>
   );
};

export default Input;

Input.propTypes = {
   placeholder: PropTypes.string,
};
