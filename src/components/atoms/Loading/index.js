import React from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import styles from './Loading.component.style';
import { Colors } from '@utils';
import Typography from '../Typography';

const Loading = () => {
   return (
      <View style={styles.wrapper}>
         <ActivityIndicator size="large" color={Colors.secondary} />
         <Typography size={14} color={Colors.background.white} style={styles.text}>
            Loading...
         </Typography>
      </View>
   );
};

export default Loading;
