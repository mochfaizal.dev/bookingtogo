import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '@utils';

export default StyleSheet.create({
   wrapper: {
      width: '100%',
      height: '100%',
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.background.blackTransparent,
   },
   text: {
      marginTop: 16,
   },
});
