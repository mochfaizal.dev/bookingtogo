import React from 'react';
import styles from './Card.component.style';
import { TouchableOpacity } from 'react-native';

const Card = (props) => {
   const { children, onClick, style } = props;
   return (
      <TouchableOpacity
         style={style ? [styles.root, style] : styles.root}
         onPress={onClick}
         disabled={!onClick}
      >
         {children}
      </TouchableOpacity>
   );
};

export default Card;
