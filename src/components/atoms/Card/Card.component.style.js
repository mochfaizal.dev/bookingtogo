import { StyleSheet } from 'react-native';
import { Colors } from '@utils';

export default StyleSheet.create({
   root: {
      borderWidth: 1,
      borderColor: Colors.line,
      borderRadius: 8,
      paddingHorizontal: 12,
      paddingVertical: 8,
   },
});
