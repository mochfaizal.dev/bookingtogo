import { Dimensions, StyleSheet } from 'react-native';
import { Colors } from '@utils';
const { width } = Dimensions.get('window');
export default StyleSheet.create({
   container: {
      borderTopStartRadius: 10,
      borderTopEndRadius: 10,
      paddingVertical: 10,
      paddingHorizontal: 20,
   },
   dragIcon: {
      minWidth: width / 3,
      minHeight: 3,
      backgroundColor: Colors.line,
   },
});
