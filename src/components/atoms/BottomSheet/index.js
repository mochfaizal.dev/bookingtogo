import { Colors } from '@utils';
import React, { forwardRef } from 'react';
import RBSheet from 'react-native-raw-bottom-sheet';
import styles from './BottomSheet.component.style';

const BottomSheet = forwardRef((props, ref) => {
   const {
      height,
      container,
      draggableIcon,
      closeOnDragDown = true,
      closeOnPressMask,
      children,
      closeOnPressBack = true,
      onClose,
      onOpen,
      keyboardAvoidingViewEnabled,
   } = props;

   return (
      <RBSheet
         ref={ref}
         closeOnDragDown={closeOnDragDown}
         closeOnPressMask={closeOnPressMask ? closeOnPressMask : false}
         height={height ? height : 500}
         closeOnPressBack={closeOnPressBack}
         customStyles={{
            wrapper: {
               flex: 1,
               backgroundColor: Colors.background.blackTransparent,
            },
            container: container ? [styles.container, container] : styles.container,
            draggableIcon: draggableIcon ? [styles.dragIcon, draggableIcon] : styles.dragIcon,
         }}
         closeDuration={350}
         onClose={onClose}
         keyboardAvoidingViewEnabled={keyboardAvoidingViewEnabled}
         onOpen={onOpen}
      >
         {children}
      </RBSheet>
   );
});

export default BottomSheet;
