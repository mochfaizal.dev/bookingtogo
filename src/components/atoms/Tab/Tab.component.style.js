import { Colors, Fonts } from '@utils';
import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
   root: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingStart: width / 2.5,
      paddingEnd: width / 2.5,
   },
   seperator: {
      width: 14,
      height: 2,
      backgroundColor: Colors.main,
      marginHorizontal: 12,
      alignSelf: 'center',
      borderRadius: 8,
   },
   backgroundNumber: (isActive) => ({
      backgroundColor: isActive ? Colors.main : Colors.background.inactive,
      width: 20,
      height: 20,
      borderRadius: 20 / 2,
      alignItems: 'center',
      justifyContent: 'center',
   }),
});
