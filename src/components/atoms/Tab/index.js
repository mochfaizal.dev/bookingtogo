import React, { forwardRef } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import Typography from '../Typography';
import Gap from '../Gap';
import { Colors } from '@utils';
import styles from './Tab.component.style';

const Tab = forwardRef((props, ref) => {
   const { data, onClick } = props;

   return (
      <FlatList
         ref={ref}
         data={data}
         horizontal={true}
         showsHorizontalScrollIndicator={false}
         contentContainerStyle={styles.root}
         ItemSeparatorComponent={<View style={styles.seperator} />}
         renderItem={({ item, index }) => {
            return (
               <TouchableOpacity
                  key={index}
                  style={{ flexDirection: 'row', alignItems: 'center' }}
                  onPress={() => onClick(item.name)}
               >
                  <View style={styles.backgroundNumber(item.is_active)}>
                     <Typography size={10} color={Colors.background.white}>
                        {index + 1}
                     </Typography>
                  </View>
                  <Gap width={12} />
                  <Typography
                     size={13}
                     color={item.is_active ? Colors.text.primary : Colors.background.inactive}
                  >
                     {item.name}
                  </Typography>
               </TouchableOpacity>
            );
         }}
      />
   );
});

export default Tab;
