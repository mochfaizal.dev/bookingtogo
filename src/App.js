import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import Routers from '@routers';
import { SafeAreaView, StatusBar } from 'react-native';
import { Colors } from './utils';
import { Loading } from '@components';
import { Provider, useSelector } from 'react-redux';
import store from '@store';
import dayjs from 'dayjs';
require('dayjs/locale/id');

//Set default local date dayjs
dayjs.locale('id');

const MainApp = () => {
   const { General } = useSelector((state) => state);
   return (
      <NavigationContainer>
         <StatusBar backgroundColor={Colors.main} />
         <SafeAreaView style={{ flex: 1, display: 'flex' }}>
            <Routers />
         </SafeAreaView>
         {General.loading && <Loading />}
      </NavigationContainer>
   );
};

const App = () => {
   return (
      <Provider store={store}>
         <MainApp />
      </Provider>
   );
};

export default App;
