# Test React Native Developer - Bookingtogo.com

App Name - Booking

## Table of Content:

-  [Technologies](#technologies)
-  [Setup](#setup)

## Technologies

-  React Native
-  Axios
-  Redux
-  Redux Thunk
-  Atomic design pattern
-  SVG Transform
-  Dayjs
-  Babel Module Resolver
-  React Native Async Storage

## Setup

-  download or clone the repository
-  run `yarn install`
-  create file .env.development from .env.example
-  running app with `yarn android`

MIT license @(Moch Faizal)
